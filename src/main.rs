mod print;
mod vars;
mod types;

fn main() {
    println!("Hello, world! from main.rs");

    // import modules from others .rs files, 
    // name of file without extntion then :(x2) and finally the name of the funtion.
    print::prformat();
    vars::varformat();
    types::run()
}
