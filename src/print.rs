pub fn prformat(){
    // print to console
    println!("Hello From print.rs file");
    
    // basic formatting, can't println a integer, must be formatting
    // "{}", 1
    println!("Number: {}", 1);

    // positional arguments, add th index number to a foratting string
    println!("{0} is from {1} and {0} likes to {2}", "javier", "mexico", "code");

    // Named arguments, also can have a better formatting when it becomes a long formatting string.
    println!("{name} likes to play {activity}",
             name = "Lia", 
             activity = "drums"
             );

    // Placeholders traits
    println!("Binary: {:b} Hex: {:x} Octal: {:o}", 25, 10, 10);

    // Placeholder for debug trait
    println!("{:?}", (12, true, "Hello"));

    // Basic math
    println!("10 + 10 = {}", 10 + 10)
}
