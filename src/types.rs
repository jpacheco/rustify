/*
Primitives types:
Integers -> u8, i8, u16, u16 .. 64, 32, 128 (number of bits they take in memory)
Floats -> f32, f64
Boolean -> (bool: true/false)
Characters -> (char)
Tupples
Arrays
*/

/*
Rust is a typd language, Which menas that it must know the types of all variables
at compile time, however, the compilder can usually infer what type we want to use
based on the value an how we use it.
*/

pub fn run() {
    // Default is "i32"
    let x = 1;

    //Default is "f64"
    let y = 2.5;

    // Add explicit type
    let z: i64 = 45454545454545;

    //Find max size:
    print!("{x}\n{y}\n{z}\n");
    println!("Max i32: {}", std::i32::MAX);
    println!("Max i64: {}", std::i64::MAX);

    // Boolean
    let is_active: bool = true;

    //Get boolean from expression.
    let is_greater: bool = 10 < 5;
    
    // Characters
    let a1: char = 'a';
    let face = "\u{1F600}";
    
    println!("{:?}", (x, y, z, is_active, is_greater, a1, face))
}
