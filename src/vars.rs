// Variables hold primitive data or references to data
// Variables are immutable y default
// Rust is a block-scoped langauage

pub fn varformat() {
    let name = "Brad";

    // keyword "mut" make a variabe mutable, but if changes,  we would have some warnings !!!
    let mut age = 37;
    println!("My name is {name} and i am {age}");

    age = 38;

    println!("My name is {name} and i am {age}");

    // Define constants
    const ID: i32 = 001;
    println!("ID: {}", ID);

    // Assign multiple vars
    let ( my_name, my_age ) = ( "Javier", 31 );
    println!("My name is {my_name} im {my_age} years old");
}
